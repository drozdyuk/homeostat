import akka.actor.{ActorLogging, Actor}
import akka.pattern.{pipe,ask}
import scala.concurrent.duration._
import scala.util.Random


object Homeostat {
  case class Input(ss: List[Float])
  case class Output(result: Float, stable: Boolean)
}

class Homeostat(N: Int,
                val delta: Float,
                var weights: List[Float],
                var xs: List[Float],
                var ys: List[Float]) extends Actor with ActorLogging {

  import Homeostat._

  val limits = (0.5 - delta, 0.5 + delta)

  def receive = {
    case Input(inputs) =>

      val sum = weightedSum(inputs)
      log.debug(s" >>> Input is $inputs")
      val s = computeOutput(sum)
      log.debug(s" <<< Output is $s")
      var stable = false
      if ( outsideLimits(s) ){
        stable = false
        log.debug(" --- Unstable -> Adapting.")
        adapt()
      }else{
        stable = true
        log.debug(" +++ Stable.")
      }


      sender ! Output(s, stable)
  }

  /**
   * Pick new random values for the behaviour
   */
  def adapt(){
    weights = List.fill(N)(Random.nextFloat)
    xs = ((xs.head :: List.fill(N-2)(Random.nextFloat * N)) :+ xs.last).sorted
    ys = List.fill(N)(Random.nextFloat).sorted
  }


  def outsideLimits(s:Float):Boolean = {
    (s < limits._1 || s > limits._2)
  }

  def weightedSum(inputs:List[Float]): Float = {
    inputs.zip(weights).foldLeft(0.0f) {
      case (acc, (s, w)) => acc + s * w
    }
  }

  def computeOutput(sum:Float) = {
    // Fetch indices for x's and y's
    val (from, to) = findRange(sum)
    val x1 = xs(from)
    val x2 = xs(to)
    val y1 = ys(from)
    val y2 = ys(to)

    y1 + (y2 - y1)*(sum - x1)/(x2 - x1)

  }

  /**
   * Find the range of indicies that the sum falls into.
   * I.e. if   x1 <= I < x2
   * it will return (1, 2)
   * @param sum
   * @return
   */
  def findRange(sum: Float): (Int, Int) = {
    val range = xs.zip(xs.drop(1)).zipWithIndex.find {
      case (((from, to), i)) => sum >= from && sum < to
    }.map {
      case ((from, to), i) => (i, i + 1)
    }
    if (range.isEmpty) throw new IllegalStateException("Sum did not fall within range")
    range.get
  }
}
