import akka.actor.{ ActorRef, ActorSystem, Props, Actor, Inbox }
import akka.util.Timeout
import java.util.concurrent.TimeUnit
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.Random
import akka.pattern.ask


object Main extends App {

  // Create the 'helloakka' actor system
  val system = ActorSystem("helloakka")

  val N = 2
  val delta = 0.1f
  val weights = List.fill(N)(Random.nextFloat)
  val xs = ((0.0f :: List.fill(N-2)(Random.nextFloat)) :+ N.toFloat).sorted
  val ys = List.fill(N)(Random.nextFloat()).sorted
  val homeostat = system.actorOf(Props(new Homeostat(N, delta, weights, xs, ys)), "A")

  val inputs = List.fill(N)(Random.nextFloat())

  import Homeostat._
  implicit val timeout = Timeout(100, TimeUnit.MILLISECONDS)

  // Case1: Move arrow and wait for homeostat to come back
  // Case2: Incessantly move the arrow

  //val input2 = List(0.7f, 0.3f, 0.4f, 0.1f)
  //val Output(out, stable) = Await.result(homeostat ? Input(input2), 100.millisecond)
  var go = true
  val input = List.fill(1)(Random.nextFloat())
  while(go){
    val Output(out, stable) = Await.result(homeostat ? Input(input), 100.millisecond)
    if(stable) { go =false;}
  }
  Await.result(homeostat ? Input(List(0.5f)), 100.millisecond)
  Await.result(homeostat ? Input(List(0.5f)), 100.millisecond)




  print("DONE")
  /*
  val homeostat2 = system.actorOf(Props(new Homeostat(N, delta, weights, xs, ys)), "B")

  var keepGoing = true
  // Start off with initial inputs
  var lastInputs = inputs
  while(keepGoing){
    val h1Inputs = lastInputs
    val Output(out1, stable) = Await.result(homeostat ? Input(h1Inputs), 100.millisecond)
    val h2Inputs = List(out1, lastInputs(1))

    val Output(out2, stable2) = Await.result(homeostat2 ? Input(h2Inputs), 100.millisecond)
    lastInputs = List(h2Inputs(0), out2)

    if (stable && stable2) { keepGoing = false}
  }
*/
  //val greetPrinter = system.actorOf(Props[GreetPrinter])
  // after zero seconds, send a Greet message every second to the greeter with a sender of the greetPrinter
  //system.scheduler.schedule(0.seconds, 1.second, greeter, Greet)(system.dispatcher, greetPrinter)


}
